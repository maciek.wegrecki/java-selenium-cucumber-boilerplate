package mw.webelement;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Based on http://www.mograblog.com/2013/08/extending-selenium-in-java.html
 * GitHub: https://github.com/GuyMograbi/mograblog-selenium-extension
 */
public abstract class MWElement {

    protected WebElement rootElement;

    protected WebDriver webDriver;

    /**
     * @param WebElement rootElement
     */
    public void setRootElement(WebElement rootElement) {
        this.rootElement = rootElement;
    }

    /**
     * @return WebElement rootElement
     */
    public WebElement getRootElement() {
        return this.rootElement;
    }

    /**
     * @param WebDriver webDriver
     */
    public void setWebDriver(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    /**
     * Warning that method should be used only when using xpath to select the rootElement.
     *
     * @return String
     */
    protected String getXPathFromWebElement() {
        return rootElement.toString().split("->")[1].replaceFirst("(?s)(.*)\\]", "$1" + "").split("xpath: ")[1];
    }
}
