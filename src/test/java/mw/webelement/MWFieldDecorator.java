package mw.webelement;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;

import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.DefaultFieldDecorator;
import org.openqa.selenium.support.pagefactory.ElementLocator;
import org.openqa.selenium.support.pagefactory.FieldDecorator;

import java.lang.reflect.Field;

public class MWFieldDecorator implements FieldDecorator {

    final DefaultFieldDecorator defaultFieldDecorator;

    final SearchContext searchContext;
    private final WebDriver webDriver;

    public MWFieldDecorator(SearchContext searchContext, WebDriver webDriver) {
        this.searchContext = searchContext;
        this.webDriver = webDriver;
        defaultFieldDecorator = new DefaultFieldDecorator(new DefaultElementLocatorFactory(searchContext));
    }

    public Object getEnhancedObject(Class clzz, MethodInterceptor methodInterceptor){
        Enhancer e = new Enhancer();
        // We could do a better abstraction here..
        // We could use a factory to return the Implementing class for each type.
        // For example, we could define SelectComponent, and map it to MograblogSelectComponent
        // in the factory.
        e.setSuperclass(clzz);
        e.setCallback(methodInterceptor);

        return e.create();
    }

    @Override
    public Object decorate(ClassLoader loader, Field field) {
        if (MWElement.class.isAssignableFrom(field.getType())  && field.isAnnotationPresent(FindBy.class)) {
            return getEnhancedObject(field.getType(), getElementHandler(field));
        } else {
            return defaultFieldDecorator.decorate(loader, field);
        }
    }

    private MWLocator.ElementHandler getElementHandler(Field field) {
        return new MWLocator.ElementHandler(field, getLocator(field), webDriver);
    }

    private ElementLocator getLocator(Field field) {
        return new DefaultElementLocatorFactory(searchContext).createLocator(field);
    }
}
