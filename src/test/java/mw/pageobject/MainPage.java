package mw.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class MainPage {
    public MainPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.NAME, using = "q")
    private WebElement searchInput;

    @FindBy(how = How.XPATH, using = "//input[@value='Google Search']")
    private WebElement searchButton;

    /**
     * @param String password
     */
    public void search(String keyword) {
        searchInput.sendKeys(keyword);
        searchButton.click();
    }
}
