package mw.env;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.Sources;

@Sources({
    "classpath:dev.properties" // mention the property file name
})
public interface Environment extends Config {
    String url();
    String driver();
}
