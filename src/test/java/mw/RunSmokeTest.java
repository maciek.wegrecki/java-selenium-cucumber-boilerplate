package mw;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

// Code style guide https://google.github.io/styleguide/javaguide.html

@RunWith(Cucumber.class)
@CucumberOptions(
    plugin = {"pretty","html:reports/test-report"},
    tags = "@smokeTest",
    glue = {"mw.steps"}
)
public class RunSmokeTest {
}
