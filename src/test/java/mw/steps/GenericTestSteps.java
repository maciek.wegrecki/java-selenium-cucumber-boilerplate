package mw.steps;

import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;

import mw.pageobject.MainPage;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class GenericTestSteps extends AbstractTest {
    ChromeDriver driver = getDriver();

    public void navigate(String path) {
        driver.get(URL + path);
    }

    @When("^user goes to google$")
    public void goesToGoogle() {
        driver.get(URL);
    }

    @And("^search for \"([^\"]*)\"$")
    public void searchFor(String keyword) {
        MainPage homepage = new MainPage(driver);
        homepage.search(keyword);
    }

    @Then("^user can see \"([^\"]*)\"$")
    public void canSee(String keyword) {
        driver.getPageSource().contains(keyword);
    }

    @After
    public void tearDown()
    {
        driver.getLocalStorage().clear();
        driver.quit();
    }
}
