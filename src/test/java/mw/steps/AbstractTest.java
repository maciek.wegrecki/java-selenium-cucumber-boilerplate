package mw.steps;

import java.util.concurrent.TimeUnit;

import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import mw.env.Environment;

public abstract class AbstractTest {
    public static String URL;
    public static String chromeDriverPath;

    protected static ChromeDriver driver = null;

    public AbstractTest() {
        Environment env = ConfigFactory.create(Environment.class);
        URL = env.url();
        chromeDriverPath = env.driver();

        System.setProperty("webdriver.chrome.driver", chromeDriverPath);
    }

    public ChromeDriver getDriver() {
        if (driver == null) {
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--headless", "--no-sandbox", "--disable-dev-shm-usage", "--disable-gpu", "--window-size=1920,1200","--ignore-certificate-errors");

            driver = new ChromeDriver(options);
            driver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
        }

        return driver;
    }
}
