# Running headless on server:

## 1. Install chrome: (only on a server)
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
dpkg -i google-chrome*.deb
apt install -f

## 2. Download chrome driver based on above chrome version (https://chromedriver.chromium.org/downloads): (skip if you have chrome driver)
wget https://chromedriver.storage.googleapis.com/ VERSION /chromedriver_linux64.zip
unzip chromedriver_linux*.zip
mv chromedriver /usr/local/bin/

## 3. Install maven: (skip if you have maven)
apt install maven

## 4. Install JDK: (skip if you have jdk)
apt install default-jdk

## 5. Install packages:
mvn install

## Run test:
mvn test
